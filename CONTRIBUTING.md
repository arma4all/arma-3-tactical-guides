# How to contribute
First of all, thank you for taking the time to contribute to this collection. We've tried to make it as complete as possible and are constantly trying to correct errors and keep the guides up to date. You can help us do more.

## Getting started

### Check out the roadmap

We have some wishes for the collection in mind and we have issued them. If there is an error (of any type) or something that is not listed in the **issues** page or there is no one assigned to the issue, feel free to correct/add it! Please consider discussing it in the issue or create a new issue for it so there is no conflicting information.

### Writing some text!

Contributing to a project on GitLab is pretty straight forward. If this is you're first time, these are the steps you should take.

- Fork this repo.

And that's it! Read the available text and change the part you don't like! Your change should be consistent with the current text and not conflict with current information. If this is the case please take some time to correct the conflicting parts too.

If you're adding new information, start from the latest branch. It would be a better practice to create a new branch and work in there.

When you're done, submit a pull request and for one of the maintainers to check it out. We would let you know if there is any problem or any changes that should be considered.

### Informed changes

We haven't prepared any text templates, but please spend some time documenting your corrections/additions before pulling a merge request. This could potentially save a lot of people some trouble.

### Documentation

It is most desirable to provide as many references to your writings as possible. This does not mean you have to provide references for every line your write, this is not an academic text. Still, it could prove to be extremely beneficial to provide references to your writings for future correction or improvements.
