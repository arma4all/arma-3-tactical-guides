# Version 1.1
## Additions
- Information about the project has been added to all guides at the Acknowledgements section

## Information changes
- Basic Combat Training
  - Added a chapter about patrolling (Chapter "17. Patrolling")
  - Updated the information to accommodate the new ACE 3 medical framework
    - The addition of splints
    - Morphine use and pain management

# Version 1.0.4.1
## Format changes
- All guides
  - All chapters now start on a new page

## Information changes
- Basic Combat Training
  - Added information regarding pie-ing (Chapter "17.5.1 Movement in Urban Areas")
  - Added information about stacking, buttonhook, crossing (Chapter "17.5.3.5 Techniques for Entering Buildings and Clearing Rooms")
  - Added image for grenade throwing into room (Chapter "17.5.3.5 Techniques for Entering Buildings and Clearing Rooms")
  - Added L-Shaped hallway clearing (Chapter "17.5.3.8")
  - For more information and the contributor of each change, see the "Release Notes.txt" of Basic Combat Training guide

# Version 1.0.4
The collection has been developed prior to its exposure on GitLab, so this is the initial version available to everyone.

For more information on the changes of each individual guide, see the corresponding "Release Notes.txt" files accompanying the guides.
