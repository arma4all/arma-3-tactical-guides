# ArmA 3 Tactical Guides

A collection of free and "open-source" tactical guides for ArmA 3

At the moment the collection includes:
- Basic Combat Training
- Grenadier
- Automatic Rifleman
- Explosives Ordnance Disposal (EOD)

Work-in-Progress are:
- Fireteam Leader

Planning for future releases:
- (Dedicated) Marksman
- Medic
- Radio-Telephone Operator (RTO)
- Squad leader
- Platoon Commander

## Important note about the guides
These guides are COMPLETELY free of copyrights and warranties and can be used in ANY way anyone sees fit. This means you can copy it, adapt it to your needs (possibly your unit’s needs), not use it at all, put it somewhere online, get money from it, give money for it, don’t give a shit about it, or literally do whatever you want with it.

For more information see **LICENSE**.
