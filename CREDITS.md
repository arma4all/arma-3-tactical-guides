# Credits
Here you can find credits to all contributors, direct or indirect.

# Info
- Direct contributors are those who have directly contributed to the development of the guides. Direct contribution may be either writing of even one sentence, correction of some kind, or even active discussion on possible corrections, directions, or suggestions.
- Indirect contributors are those who haven't actively participated in the development of the guides, but their work has been used for the development of the guides, either with or without direct permission.

# Contributors
## Direct contributors
- **Aris**
  - Kick started the whole project
  - Wrote the biggest part of all the guides.
    - Basic Combat Training
      - The whole text apart from Chapters 8, 16, 17, 18 and 19
    - Grenadier
      - The whole text
    - Automatic Rifleman
      - The whole text
    - Explosives Ordnance Disposal
      - Chapters 1, 2, 3, 4 and part of 7
    - Fireteam Leader
      - The whole text
- **Achilles**
  - Performed most corrections and maintenance of the texts
    - Spelling and some phrasing corrections
  - Wrote parts of some guides
    - Basic Combat Training guide
      - Chapters 8, 16, 17, 18 and 19
    - Explosive Ordnance Disposal
      - Chapters 5, 6 and part of 7
